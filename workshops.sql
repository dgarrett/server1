DROP DATABASE IF EXISTS workshops;
CREATE DATABASE  workshops;
\c workshops;

DROP TABLE IF EXISTS enrolled;
CREATE TABLE enrolled (
  workshop varchar(35) NOT NULL default '',
  attendee varchar(35) NOT NULL default ''
);

INSERT into enrolled (workshop, attendee) values ('Java Essentials', 'Joe Smith');
INSERT into enrolled (workshop, attendee) values ('React Fundamentals', 'Ane Wurley');
INSERT into enrolled (workshop, attendee) values ('React Fundamentals', 'Jowel Campos');
INSERT into enrolled (workshop, attendee) values ('React Fundamentals', 'Antony O''Neal');
INSERT into enrolled (workshop, attendee) values ('CPSC 350', 'Joe Smith');
INSERT into enrolled (workshop, attendee) values ('CPSC 350', 'Joe Smith');
INSERT into enrolled (workshop, attendee) values ('Java Essentials', 'Joe Smith');
INSERT into enrolled (workshop, attendee) values ('Java Essentials', 'Joe Smith');

DROP Role if EXISTS headmaster;
create user headmaster with password 'SoosMootj!17!';
Grant select, insert, delete on enrolled to headmaster;