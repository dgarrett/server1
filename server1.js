/*let results = {atendees: []};
  results.atendees = response.rows.map(function(item){
  return item.attendee;
  });

  console.log(JSON.stringify(results));
  res.json(results);
 */
const express = require('express');
var Pool = require('pg').Pool;
var bodyParser = require('body-parser');


const app = express();
var config = {
host: 'localhost',
	  user: 'headmaster',
	  password: 'SoosMootj!17!',
	  database: 'workshops',
};

var pool = new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));


//get request


app.get('/api', async (req, res) => { //require and response

		var workshop = req.query.workshop;

		//error checking for workshop
		if(!workshop || (workshop === "")){
		//error message
		var allWorkshops = await pool.query('select distinct workshop from enrolled;');

		let available = {workshops: []};
		available.workshops = allWorkshops.rows.map(function(item){
			return item.workshop;
			});

		console.log(JSON.stringify(available));
		res.json({error: 'workshop not given. List of available workshops: ', workshops: available.workshops});

		}
		
		else{
			try{
				var checkAll = await pool.query("select distinct workshop from enrolled;")
				
				let available = {workshops: []};
				available.workshops = checkAll.rows.map(function(item){
					return item.workshop;
				});
				
				
				if(!available.workshops.includes(workshop)){
					console.log(JSON.stringify(available));
					res.json({error: 'workshop not found. List of available workshops: ', workshops: available.workshops});
		}
		
		//contact server only after error checking

		else{
			//variable substitution 
			var response = await pool.query('select * from enrolled where workshop = $1;', [workshop]);
			//
			let results = {attendees: []};
			results.attendees = response.rows.map(function(item){
					return item.attendee;
					});

			console.log(JSON.stringify(results));
			res.json(results);
		}

		
		}catch(e){
			console.error('Error running query ' + e);
		}
		}
		
});

//post request

app.post('/api', async (req, res) => {
		//access body
	var attendee = req.body.attendee ;
	var workshop = req.body.workshop;

	
	if(!attendee || !workshop){
		//error "no paramaters"
		res.json({error: 'paramaters not given'});
	}
	else{
		try{
			var enrolledAttendee = await pool.query('select * from enrolled where workshop = $1 and attendee = $2;', [workshop, attendee]);
			
			if(enrolledAttendee.rowCount != 0){
				//attendee is enrolled
				res.json({error: 'attendee already enrolled'});
			}
			else{
				//add attendee to workshop
				var response = await pool.query('insert into enrolled values ($1, $2)', [workshop, attendee]);
				res.json({attendee, workshop});
			}
		}
		catch(e){
		console.log('Error posting', e);
		}
	}
});
	

//delete request

app.delete('/api', async (req, res) => {
		//access body
		console.log(req.body);
		var item = req.body.item ;
		var priority = req.body.priority;
		if (!item || !priority){
		res.json({error: 'paramaters not given'});
		}
		else {
		try {
		var response = await pool.query('delete from into enrolled where task = $1 and priority = $2', [item, priority]);
		//respond
		res.json({status: 'deleted'});
		}
		catch(e){
		console.log('Error running delete', e);
		}
		}
		});


//port running

app.listen(app.get('port'), () => {
		console.log('Running');
		})


























